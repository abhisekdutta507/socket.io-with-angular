import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket: SocketIOClient.Socket;
  /**
   * @description data to be tracked
   */
  private store: StoreData = {};
  private subject: BehaviorSubject<StoreData> = new BehaviorSubject(this.store);
  public observable$: Observable<StoreData> = this.subject.asObservable();

  constructor() {
    this.socket = io(environment.BASE_URL);
    this.socket.on('connect', this.connect.bind(this));
    this.socket.on('disconnect', this.disconnect.bind(this));
    this.socket.on('error', this.error.bind(this));
    this.socket.on('loggedIn', this.loggedIn.bind(this));
    this.socket.on('rendered', this.rendered.bind(this));
  }

  emit(type: string, data: any) {
    this.socket.emit(type, data);
  }

  connect() {
    const store = {
      type: 'connect',
      id: this.socket.id,
      payload: {}
    };
    this.subject.next(store);
  }

  disconnect() {
    const store = {
      type: 'disconnect',
      id: this.socket.id,
      payload: {}
    };
    this.subject.next(store);
  }

  error(props: any) {
    const store = {
      type: 'error',
      id: this.socket.id,
      payload: props
    };
    this.subject.next(store);
  }

  loggedIn(props: any) {
    const store = {
      type: 'loggedIn',
      id: this.socket.id,
      payload: props
    };
    this.subject.next(store);
  }

  rendered(props: any) {
    const store = {
      type: 'rendered',
      id: this.socket.id,
      payload: props
    };
    this.subject.next(store);
  }
}

export interface StoreData {
  id?: string;
  type?: string;
  payload?: any;
}
