import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { SocketService, StoreData } from './services/socket.io.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public username: string = '';
  public title = 'Socket.io';
  public logged: boolean = false;
  public conntected: boolean = false;
  public loader: boolean = false;

  private subscriptions: Subscription[] = [];

  constructor(private socket: SocketService) {
    const sn: Subscription = this.socket.observable$.subscribe(this.onStoreUpdate.bind(this));
    this.subscriptions.push(sn);
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.subscriptions.forEach((sn: Subscription) => {
      sn.unsubscribe();
    });
  }

  login() {
    this.socket.emit('login', { username: this.username });
  }

  start() {
    this.loader = true;
    this.socket.emit('render-video', { username: this.username });
  }

  connect(store: StoreData) {
    this.conntected = true;

    if (this.username) {
      this.login();
    }
  }

  disconnect(store: StoreData) {
    this.conntected = false;
    this.logged = false;
  }

  error(store: StoreData) {
    this.loader = false;
    console.log(store);
  }

  loggedIn(store: StoreData) {
    this.logged = true;
    console.log(store);
  }

  rendered(store: StoreData) {
    this.loader = false;
  }

  onStoreUpdate(store: StoreData) {
    switch (store.type) {
      case 'connect': {
        this.connect(store); break;
      }

      case 'disconnect': {
        this.disconnect(store); break;
      }

      case 'error': {
        this.error(store); break;
      }

      case 'loggedIn': {
        this.loggedIn(store); break;
      }

      case 'rendered': {
        this.rendered(store); break;
      }

      default: {
        console.log('store initialised');
      }
    }
  }
}
